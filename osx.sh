#!/bin/bash

echo "Changing the dock to autohide"
defaults write com.apple.dock autohide -bool true

echo "Making hidden applications translucent in the dock"
defaults write com.apple.dock showhidden -bool true

echo "Making Finder show all file extensions"
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

echo "Expanding save panel"
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true

echo "Restoring cmd-D shortcut"
defaults write NSGlobalDomain NSSavePanelStandardDesktopShortcutOnly -bool true

echo "Expanding print panel"
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true

echo "Setting Printer App to close after printing"
defaults write ~/Library/Preferences/com.apple.print.PrintingPrefs "Quit When Finished" -bool true

echo "Enabling stack hightlight effect"
defaults write com.apple.dock mouse-over-hilte-stack -bool true

echo "Enabling dock spring loading"
defaults write enable-spring-load-actions-on-all-items -bool true

echo "Enabling dock indicators"
defaults write com.apple.dock show-process-indicators -bool true

echo "Enabling key repeat"
defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false

echo "Disabling autocorrect"
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

echo "Disabling Lion window animations"
defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool false

echo "Turning on scrollbars"
defaults write NSGlobalDomain AppleShowScrollBars -string "Always"

echo "Disabling .DS_Store on network volumes"
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true

echo "Protecting screensaver"
defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 0

echo "Enabling Safari Debug menu"
defaults write com.apple.Safari IncludeDebugMenu -bool true

echo "Disabling Mail animations"
defaults write com.apple.Mail DisableReplyAnimations -bool true
defaults write com.apple.Mail DisableSendAnimations -bool true

echo "Disabling Restore"
defaults write com.apple.loginwindow TALLogoutSavesState -bool false 
defaults write NSGlobalDomain NSQuitAlwaysKeepsWindows -bool false

echo "Making User Library visible"
chflags nohidden ~/Library

echo "Disabling local Time Machine"
tmutil disablelocal

echo "Encrypting Swap Space"
defaults write /Library/Preferences/com.apple.virtualMemory DisableEncryptedSwap -bool false

echo "Setting 64-bit Kernel"
systemsetup -setkernelbootarchitecture x86_64

echo "Correcting scroll direction"
defaults write ~/Library/Preferences/.GlobalPreferences com.apple.swipescrolldirection -bool false

echo "Securing Sleep Mode"
pmset -a destroyfvkeyonstandby 1 hibernatemode 25

echo "Expanding Crash Dialogs"
defaults write com.apple.CrashReporter DialogType developer

echo "Disabling Dashboard"
defaults write com.apple.dashboard mcx-disabled -bool true

echo "Changing Screen Capture format to PNG"
defaults write com.apple.screencapture type png

echo "Enabling AirDrop on all interfaces"
defaults write com.apple.NetworkBrowser BrowseAllInterfaces 1

echo "Setting Login Window to display System verison"
defaults write /Library/Preferences/com.apple.loginwindow SystemVersion

echo "Relaunching the Dock"
killall Dock

echo "Relaunching Finder"
killall Finder

echo "Relaunching SystemUIServer"
killall SystemUIServer
