Madison’s dotfiles
==================

Description
-----------
These files are my personal preference andconfiguration files that I use on my Mac.

###Setting Up###
To set up a new Lion machine, run `chmod 755 osx.sh` and then execute `sudo ./osx.sh` and then move the respective config files to the location you want them to be at.

####Forked####
[Original Inspiration](https://github.com/mathiasbynens/dotfiles)